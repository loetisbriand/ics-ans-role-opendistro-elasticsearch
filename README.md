# ics-ans-role-opendistro-elasticsearch

Ansible role to install opendistro-elasticsearch.

## Role Variables

```yaml
opendistro_elasticsearch_version: 1.13.2

elasticsearch_network_host: 127.0.0.1

elasticsearch_http_port: 9200

elasticsearch_template:
  - name: config
    file: elasticsearch.yml.j2
    dest: /etc/elasticsearch/elasticsearch.yml

...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-opendistro-elasticsearch
```

## License

BSD 2-clause
